package webservice;

import java.util.ArrayList;
import java.util.List;

import org.orm.PersistentException;
import org.orm.PersistentTransaction;

public class ServicioContacto {
	
	/**
	* Ingreso de Registro
	* @param oContactoVO
	* @return
	*/
	public String agregarContacto(domain.ContactoVO oContactoVO){
	PersistentTransaction t;
	try {
	t = orm.Taller1MagisterInformaticaPersistentManager.instance().getSession().beginTransaction();
	try {
	orm.Contacto lormContacto = orm.ContactoDAO.createContacto();
	// Initialize the properties of the persistent object here
	lormContacto.setNombre(oContactoVO.getNombre());
	lormContacto.setApellido(oContactoVO.getApellido());
	lormContacto.setMail(oContactoVO.getMail());
	lormContacto.setTelefono(oContactoVO.getTelefono());
	System.out.println("Ingreso Exitoso");
	orm.ContactoDAO.save(lormContacto);
	t.commit();
	return "ingreso existoso";
	}
	catch (Exception e) {
	t.rollback();
	return "Error-Rollback";
	}
	} catch (PersistentException e1) {
	// TODO Auto-generated catch block
	e1.printStackTrace();
	return "Error persistencia";
	}
	}
	
	public String eliminarContacto(domain.ContactoVO oContactoVO) {
		PersistentTransaction t;
		try {
			t = orm.Taller1MagisterInformaticaPersistentManager.instance().getSession()
					.beginTransaction();
			try {
				String query = "nombre = '"+oContactoVO.getNombre()+"'";
				orm.Contacto lormContacto = orm.ContactoDAO.loadContactoByQuery(query, null);
				// Delete the persistent object
				orm.ContactoDAO.delete(lormContacto);
				t.commit();
				
				System.out.println("Eliminacion exitosa");
				return "Eliminacion existosa";
			} catch (Exception e) {
				t.rollback();
				return "Error-Rollback";
			}
		} catch (PersistentException e1) {
			e1.printStackTrace();
			return "Error persistencia";
		}
	}
	
	public String modificarContacto(domain.ContactoVO oContactoVO, domain.ContactoVO oContactoVONuevo) {
		PersistentTransaction t;
		try {
			t = orm.Taller1MagisterInformaticaPersistentManager.instance().getSession()
					.beginTransaction();
			try {
				String query = "nombre = '"+oContactoVO.getNombre()+"'";
				orm.Contacto lormContacto = orm.ContactoDAO.loadContactoByQuery(query, null);
				if(lormContacto.getNombre().isEmpty()) {
					return "Contacto no encontrado";
				}
				
				lormContacto.setNombre(oContactoVONuevo.getNombre());
				lormContacto.setApellido(oContactoVONuevo.getApellido());
				lormContacto.setMail(oContactoVONuevo.getMail());
				lormContacto.setTelefono(oContactoVONuevo.getTelefono());
				
				orm.ContactoDAO.save(lormContacto);
				t.commit();
				
				System.out.println("Modificacion exitosa");
				return "Modificacion existosa";
			} catch (Exception e) {
				t.rollback();
				return "Error-Rollback";
			}
		} catch (PersistentException e1) {
			e1.printStackTrace();
			return "Error persistencia";
		}
		
	}
	
	/**
	* Retorna un listado de objeto de la clase ContactoVO
	* @return List<domain.ContactoVO>
	*/
	public List<domain.ContactoVO> mostrarContacto(){
	List<domain.ContactoVO> contactos = new
	ArrayList<domain.ContactoVO>();
	orm.Contacto[] ormContactos;
	try {
	ormContactos = orm.ContactoDAO.listContactoByQuery(null, null);
	int length = ormContactos.length;
	for (int i = 0; i < length; i++) {
	System.out.println(ormContactos[i]);
	contactos.add(new
	domain.ContactoVO(ormContactos[i].getNombre(),
	ormContactos[i].getApellido(),
	ormContactos[i].getMail(),
	ormContactos[i].getTelefono()));
	}
	return contactos;
	} catch (PersistentException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
	return null;
	}
	}

}
